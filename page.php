<?php get_header(); ?>
<!-- page.php is used for all wordpress 'pages' proper (not a post, or resource)
     the discovery exercises use this page template -->
<div class="full-height container">
<div class="row">
  <div class="col-xs-12">
    <h1><?php the_title(); ?></h1>
    <hr>
  </div>
</div>
<div class="row">
<div class="post-content-wrapper">
<div class="col-xs-12 col-lg-10 col-lg-offset-1">
	<?php
	while (have_posts()) {
		the_post();
		the_content();
	}
	wp_reset_postdata();
	?>
</div>
</div>
</div>

</div><!-- full-height container -->
<?php get_footer(); ?>
