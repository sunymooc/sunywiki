<?php
/*
Template Name: HomePage
*/
?>
<?php get_header(); ?>

<div class="full-height container">
<div class="row">

<?php
while (have_posts()) {
	the_post();
	the_content();
}
wp_reset_postdata();
?>
</div>
</div>
<?php get_footer(); ?>

