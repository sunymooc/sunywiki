<?php

/* theme setup (nav bar) */
//https://code.tutsplus.com/tutorials/how-to-integrate-a-bootstrap-navbar-into-a-wordpress-theme--wp-33410
require_once(get_template_directory() . '/wp-bootstrap-navwalker.php');

/* adds support for the nav menus in the header
 * administrators can change the content of the menus from the admin screen
 *   "Appearance" > "Menus"
 * We split the 'main' nav bar and the login nav bar into two, so that we can
 * position the login navbar to the far right of the header
 */
add_theme_support('menus');
add_action('init', 'suny_setup_menus');
function suny_setup_menus() {
	register_nav_menu('main', __("Primary navigation", "suny"));
	register_nav_menu('login', __("Login navigation", "suny"));
}

/* hiding the admin bar from the top of the screen when user is logged in */
add_filter('show_admin_bar', '__return_false');
/* hiding unnecessary stuff in the admin bar (top of screen) */
add_action('admin_bar_menu', 'remove_admin_bar_items', 999);
function remove_admin_bar_items($bar) {
	$bar->remove_node('wp-logo');
	$bar->remove_node('comments');
	$bar->remove_node('new-post');
	$bar->remove_node('archive');

	$bar->remove_node('user-info');
	$bar->remove_node('edit-profile');
}
/* hiding unnecessary menus in the admin panel
 * for 'regular' users (ie anyone that is not an editor, administrator)
 */
add_action('admin_menu', 'remove_admin_panel_menus', 999);
function remove_admin_panel_menus() {
	$remove = array(
		'index.php',		   // dashboard
		'edit.php',		   //posts
		'edit.php?post_type=page', // pages
		'edit-comments.php',	   // comments
		'themes.php',
		'plugins.php',
		'users.php',
		'profile.php',
		'tools.php',
		'options-general.php'
	);
	$valid_roles = array('editor', 'administrator');
	$usr = wp_get_current_user();
	if (empty($usr->roles) || !__any_in($usr->roles, $valid_roles)) {
		foreach ($remove as $rm) {
			remove_menu_page($rm);
		}
	}
}
/* return true if any elements in $needles are found to be in $haystack */
function __any_in($needles, $haystack) {
	foreach ($needles as $n) {
		if (in_array($n, $haystack)) {
			return true;
		}
	}
	return false;
}

/* Restricting access to pages for non-administrators */
add_action('admin_init', 'restrict_admin_pages');
function restrict_admin_pages() {
	$restricted = array(
		'/wp-admin/widgets.php',
		'/wp-admin/widgets.php',
		'/wp-admin/user-new.php',
		'/wp-admin/upgrade-functions.php',
		'/wp-admin/upgrade.php',
		'/wp-admin/themes.php',
		'/wp-admin/theme-install.php',
		'/wp-admin/theme-editor.php',
		'/wp-admin/setup-config.php',
		'/wp-admin/plugins.php',
		'/wp-admin/plugin-install.php',
		'/wp-admin/options-writing.php',
		'/wp-admin/options-reading.php',
		'/wp-admin/options-privacy.php',
		'/wp-admin/options-permalink.php',
		'/wp-admin/options-media.php',
		'/wp-admin/options-head.php',
		'/wp-admin/options-general.php.php',
		'/wp-admin/options-discussion.php',
		'/wp-admin/options.php',
		'/wp-admin/network.php',
		'/wp-admin/ms-users.php',
		'/wp-admin/ms-upgrade-network.php',
		'/wp-admin/ms-themes.php',
		'/wp-admin/ms-sites.php',
		'/wp-admin/ms-options.php',
		'/wp-admin/ms-edit.php',
		'/wp-admin/ms-delete-site.php',
		'/wp-admin/ms-admin.php',
		'/wp-admin/moderation.php',
		'/wp-admin/menu-header.php',
		'/wp-admin/menu.php',
		'/wp-admin/edit-tags.php',
		'/wp-admin/edit-tag-form.php',
		'/wp-admin/edit-link-form.php',
		'/wp-admin/edit-comments.php',
		'/wp-admin/credits.php',
		'/wp-admin/about.php',
	);
	$valid_roles = array('administrator');
	$usr = wp_get_current_user();
	if (empty($usr->roles) || !__any_in($usr->roles, $valid_roles)) {
		foreach ($restricted as $r) {
			if ($_SERVER['PHP_SELF'] == $r) {
				$redirect_ = get_permalink(get_page_by_title('NotFound'));
				wp_redirect($redirect_);
				die();
			}
		}
	}
}

/*
 * Fixing the 're-ordering' of categories & hierarchical taxonomies when
 * saving a post in the editor.

 * Consider the following hierarchical taxonomy:
 *       > RootCategory
 *           > subcategory

 * When editing a post and checking off 'subcategory' what will happen on the UI
 * is 'subcategory' will get 'pulled up' to the top of the hierarchy like this:
 *       > RootCategory
 *       > subcategory
 *
 * This fixes that 'feature', and keeps subcategory in the tree, where it belongs.
 */
add_filter('wp_terms_checklist_args', 'preserve_taxonomy_hierarchy', 10, 2);
function preserve_taxonomy_hierarchy($args, $post_id) {
	$args['checked_ontop'] = false;
	return $args;
}

/*
 * When user is editing a post, and a child term is selected
 * automatically select the parent term as well
 * ex)
 *    Module_1
 *       category
 * When user selects category1, Module1 will also be selected
 * we do this whenever a user saves a 'post'
 */
add_action('save_post', 'assign_parent_terms', 10,2);
function assign_parent_terms($post_id, $post) {
	if ($post->post_type != 'resource') {
		return $post_id;
	}
	// automatically add parent terms if a child is specified
	$taxes = get_object_taxonomies('resource'); // array of taxonomy slugs: not objects
	foreach ($taxes as $tax_name) {
		$terms = wp_get_post_terms($post_id, $tax_name);
		foreach ($terms as $term) {
			// walk up the tree until there are no parents left
			while ($term->parent != 0 && !has_term($term->parent, $tax_name, $post)) {
				wp_set_post_terms($post_id, array($term->parent), $tax_name, true);
				$term = get_term($term->parent, $tax_name);
			}
		}
	}
}
/* here, we add some names to Wordpress's list of known query variables
 * (things we can send in a GET/POST request).

 * using 'module' we will change the sub-categories shown in the search
 * page with respect to the chosen module
 * ex) http://site.com/search/?module=LifelongLearning
 * will limit selections in the dropdown menus to those relevant to lifelong learning
 *
 * 'random' specifies whether the search page should show random results.
 */
add_filter('query_vars', 'add_query_variables');
function add_query_variables($vars) {
	$vars[] = "module";
	$vars[] = "random";
	return $vars;
}

/* support for post thumbnails/images.
 * post thumbnails are shown in search results, as well as on the page for the post (single.php)
 */
add_theme_support('post-thumbnails');

/*
 * CSS and javascript hooks - tell wordpress to load this code for every page.
 * we separate these out and call them conditional on the current page
 * because some pages do not need all of these stylesheets/scripts.
 * see the switch statement in my_assets().
 */
function include_jquery() {
	wp_deregister_script('jquery');
	wp_register_script('jquery', "https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js");
	wp_enqueue_script('jquery');
}
function include_bootstrap() {
	wp_enqueue_style("bootstrap", "https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css");
	wp_enqueue_script("bootstrap","https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js",
				null, null, true);
}
/* bootstrap-select is used for the dropdown menus on the searchpage */
function include_bootstrap_select() {
	wp_enqueue_style("bootstrap-select", "https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/css/bootstrap-select.min.css");
	wp_enqueue_script("bootstrap-select","https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/js/bootstrap-select.min.js",
			null, null, true);
}
/* stylesheets specific to each page template.
 * style.css is common to all pages. */
function include_my_styles() {
	$dir = get_stylesheet_directory_uri();
	wp_enqueue_style("searchpage", "$dir/css/searchpage.css");
	wp_enqueue_style("style", "$dir/css/style.css");
	wp_enqueue_style("header", "$dir/css/header.css");
	wp_enqueue_style("footer", "$dir/css/footer.css");
}
function include_my_scripts() {
	$dir = get_stylesheet_directory_uri();
	wp_enqueue_script("sprintf", "$dir/js/sprintf.js");        /* C-style sprintf */
	wp_enqueue_script("fixinputs", "$dir/js/fixinputs.js");    /* script to style <input> tags made by WPUserManager */
	wp_enqueue_script("footer", "$dir/js/footer.js");          /* dynamic resizing and alignment of the footer (vert-align) */
	wp_enqueue_script("listmobile", "$dir/js/list-mobile.js"); /* better <ul><li> on mobile devices */
}
/* 'base' includes required by everything */
function include_base() {
	include_jquery();
	include_bootstrap();
}
/* specific to searchpage.php */
function searchpage_scripts() {
	$dir = get_stylesheet_directory_uri();
	wp_enqueue_script("toggle_button_group", "$dir/js/toggle-button-group.js");
	/* make button group vertical if on a narrow screen */
}
/* home.php */
function homepage_styles() {
	$dir = get_stylesheet_directory_uri();
	wp_enqueue_style("home", "$dir/css/home.css");
}

add_action('wp_enqueue_scripts', 'my_assets');
function my_assets() {
	// here is where styles and scripts are conditionally included
	// depending on the page template. Here, order of includes matters!
	// basename($template) is the current page template (home.php searchpage.php ... etc.)
	global $template;
	$t = basename($template);
	switch ($t) {
	case "searchpage.php":
		include_base();
		include_bootstrap_select();
		include_my_styles();
		include_my_scripts();
		searchpage_scripts();
		break;
	case "home.php":
		include_base();
		include_my_styles();
		include_my_scripts();
		homepage_styles();
	default:
		include_base();
		include_my_styles();
		include_my_scripts();
		break;
	}
}
/*
 * Custom Post Types and Taxonomies
 * taxonomies:
 *  keywords (aka "tags")
 *  objectives (aka "why's")
 *  subjects (math, chemistry, physics, ...)
 * custom post types:
 *  we create a custom post type called 'resources'
 *  and associate all these new taxonomies with it.
 */

/* MODULE_TAXONOMY is an alias for the taxonomy name that holds the 5 modules
 * we don't give 'modules' its own taxonomy because it is hierarchical:
 * the 5 modules serve as the top-level 'terms' in the 'objectives'
 * taxonomy, and have their own objectives 'underneath' them
 * this allows us to show different objectives based on the 'meta objective'
 * (read module) that is currently selected.
 */
define("MODULE_TAXONOMY", "objectives");

add_action('init', 'register_custom_taxonomies', 0);
function register_custom_taxonomies() {
        $keyword_labels = array(
                "name"                  => "Keywords",
                "singular_name"         => "Keyword",
                "menu_name"             => "Keywords",
                "all_items"             => "All Keywords",
                "edit_item"             => "Edit Keyword",
                "view_item"             => "View Keyword",
                "update_item"           => "Update Keyword",
                "add_new_item"          => "Add New Keyword",
                "new_item_name"         => "New Keyword",
                "search_items"          => "Search Keywords",
                "not_found"             => "No Keywords found"
        );
        $keyword_args = array(
                "labels"                => $keyword_labels,
                "public"                => true,
                "publicly_queryable"    => true,
                "hierarchical"          => true,
                "show_ui"               => true,
                "show_in_menu"          => true,
                "description"           => "Keywords/Tags linked with resources"
        );
        $objective_labels = array(
                "name"                  => "Objectives",
                "singular_name"         => "Objective",
                "menu_name"             => "Objectives",
                "all_items"             => "All Objectives",
                "edit_item"             => "Edit Objective",
                "view_item"             => "View Objective",
                "update_item"           => "Update Objective",
                "add_new_item"          => "Add New Objective",
                "new_item_name"         => "New Objective Name",
                "search_items"          => "Search Objectives",
                "not_found"             => "No Objectives found"
        );
        $objective_args = array(
                "labels"                => $objective_labels,
                "public"                => true,
                "publicly_queryable"    => true,
                "hierarchical"          => true,
                "show_ui"               => true,
                "show_in_menu"          => true,
                "description"           => "Pedagogical Objectives (Why's)"
        );
        $subject_labels = array(
                "name"                  => "Subjects",
                "singular_name"         => "Subject",
                "menu_name"             => "Subjects",
                "all_items"             => "All Subjects",
                "edit_item"             => "Edit Subject",
                "view_item"             => "View Subject",
                "update_item"           => "Update Subject",
                "add_new_item"          => "Add New Subject",
                "new_item_name"         => "New Subject Name",
                "search_items"          => "Search Subjects",
                "not_found"             => "No Subjects found"
        );
        $subject_args = array(
                "labels"                => $subject_labels,
                "public"                => true,
                "publicly_queryable"    => true,
                "hierarchical"          => true,
                "show_ui"               => true,
                "show_in_menu"          => true,
                "description"           => "Academic Subjects"
        );
        register_taxonomy('keywords',   array('resource'), $keyword_args);
        register_taxonomy('objectives', array('resource'), $objective_args);
        register_taxonomy("subjects",   array('resource'),  $subject_args);
}
/* registering custom post types: resource */
add_action('init', 'register_custom_post_types', 0);
function register_custom_post_types() {
        $resource_labels = array(
                'name'                  => "Resources",
                'singular_name'         => "Resource",
                "menu_name"             => "Resources",
                "name_admin_bar"        => "Resource",
                "add_new"               => "Add New",
                "add_new_item"          => "Add New Resource",
                "new_item"              => "New Resource",
                "edit_item"             => "Edit Resource",
                "view_item"             => "View Resource",
                "all_items"             => "All Resources",
                "search_items"          => "Search Resources",
                "not_found"             => "No Resources Found",
                "not_found_in_trash"    => "No Resources found in trash"
        );
        $resource_args = array(
                "labels"                => $resource_labels,
                "description"           => "Educational Resources",
                "public"                => true,
                "publicly_queryable"    => true,
                "show_ui"               => true,
                "show_in_menu"          => true,
                "menu_position"         => 7,
                "rewrite"               => array("slug" => "resource"),
                "capability_type"       => "post",
                "has_archive"           => true,
                "hierarchical"          => false,
                "supports"              => array("title", "editor", "author",
                                                "excerpt","revisions","thumbnail"),
                "taxonomies"            => array("category","objective","subject")
       );
       register_post_type("resource", $resource_args);
}
//--------------------------------------------------------
// rendering post type links in archives and taxonomy pages
//
// this is the default callback function for rendering posts from a taxonomy
define('DESCRIPTION_MAX_LEN', 80);
function render_posts_callback($tax, $term_name, $post_type, $loop) {
        if ($loop->have_posts()) {
                if ($term_name != null) {
                        printf('<div class="row"><h3>%s</h3></div>', $term_name);
                }
                printf('<div class="row">');
                printf('<div class="col-md-offset-1">');
                printf("<ul>");
                while ($loop->have_posts()) {
                        $loop->the_post();
                        $ex = post_excerpt_clean($loop->post->post_content);
                        printf('<li class="archive-item"><a href="%s">%s</a> - <span>%s</span></li>',
                                get_permalink(), get_the_title(), $ex);
                }
		wp_reset_postdata();
                printf("</ul>");
                printf("</div>"); // offset
                printf("</div>"); // row
        }
}
// given a taxonomy, 'term', and a post type (e.g. resource)
// fetch the posts and render them in a list
// $render_fn is a callback function to render the results of the query
function render_posts_from_term($tax_name, $term_name, $term_slug, $post_type, $render_fn) {
       wp_reset_postdata();
       $args = array('post_type' => $post_type,
                 'tax_query' => array(
                         array(
                                 "taxonomy" => $tax_name,
                                 "field" => "slug",
                                 "terms" => $term_slug
                         ))
                  );
        $loop = new WP_Query($args);
        $render_fn($tax_name, $term_name, $post_type, $loop);
}

/* create an <ing> pointing to the default thumbnail */
function default_thumbnail($size = null, $attrs = array()) {
        if ($size === null) {
                $size = array(80,80);
        }
        $base = '<img src="/wp-content/uploads/default-thumbnail.png" ';
        $base .= "width=\"$size[0]\" height=\"$size[1]\" ";
        foreach ($attrs as $attr => $val) {
                $base .= "$attr=\"$val\"";
        }
        $base .= ">";
        return $base;
}
/* given post content, extract the first nwords
 * strip any HTML tags from the text. this is used to make short descriptions
 * that show in the search results
 */
function post_excerpt_clean($content, $nwords = 140) {
        assert($nwords > 0);
        $ex = strip_tags(strip_shortcodes($content));
        $words = explode(' ', $ex, $nwords + 1);
        if (count($words) > $nwords) {
                array_pop($words);
                array_push($words, "...");
                $ex = implode(' ', $words);
        }
        return $ex;
}
/* unfortunately, WP-PostRatings does not provide any facility
 * to display a post's ratings in a read-only manner. (ie without
 * some javascript code listening for a user to click on the rating stars).
 * So, we implement it here
 * This allows us to show post ratings (stars) in the search results
 */
function render_star_ratings($post_id, $tag = 'div') {
        $attributes = 'id="post-ratings-'.$post_id.'" class="post-ratings"';
        return "<$tag $attributes>".the_ratings_results($post_id, 0,0,0,0).'</'.$tag.'>';
}
/* die gracefully */
function render_error_loading_page() {
	$str = <<<'EOT'
<div class="full-height container">
<div class="row">
<p>
There was an error loading this page
</p>
</div>
</div>
EOT;
	return $str;
}

/*
 * add_class_($html, $class_)
 *   $html   = <element ...>content</element>
 *   $class_ = FOOBAR
 * returns:
 *   <element ... class="FOOBAR">content</element>
 * existing classes are preserved.
 */
function add_class_($html, $class_) {
	$endof_open_tag = strpos($html, ">");
	$class_ptr      = strpos($html, "class");
	if (($class_ptr !== FALSE) && $class_ptr < $endof_open_tag) {
		// this element has a 'class' attribute. add to existing classes
		$quote = strpos($html, "\"", $class_ptr);
		$html  = substr_replace($html, "$class_ ", $quote + 1, 0);
	} else {
		// does not have a 'class' attribute
		$html = substr_replace($html, " class=\"$class_\"", $endof_open_tag, 0);
	}
	return $html;
}
/*
 * MAKING <iframe> RESPONSIVE
 * when the_content is called, the actual post content will be filtered by this function.
 * we wrap some bootstrap classes (embed-responsive ...) around the <iframe> tag
 */
add_filter('the_content', 'wrap_iframe_responsive', 10, 1);
function wrap_iframe_responsive($content) {
	$regex = '~<iframe.*</iframe>~';
	preg_match_all($regex, $content, $matches);
	foreach ($matches[0] as $match) {
		$match_with_class = add_class_($match, "embed-responsive-item");
		$wrapped = '<div class="row"><div class="col-xs-12 col-md-8 col-md-offset-2"><div class="embed-responsive embed-responsive-16by9">' . $match_with_class . '</div></div></div>';
		$content = str_replace($match, $wrapped, $content);
	}
	return $content;
}
/*
 * MAKING <img> RESPONSIVE
 */
add_filter('the_content', 'img_responsive', 10, 1);
function img_responsive($content) {
	$regex = '~<img.*/>~';
	preg_match_all($regex, $content, $matches);
	foreach ($matches[0] as $match) {
		$img = add_class_($match, "img-responsive");
		$content = str_replace($match, $img, $content);
	}
	return $content;
}
?>
