<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- typekit font -->
    <style>
    @import url("https://use.typekit.net/fcm1bkn.css");
    </style>
    <title><?php echo get_bloginfo('title'); ?></title>
    <?php wp_head(); ?>
  </head>
  <body>
  <nav class="navbar navbar-default navbar-static-top">
  <div id="navbar-container" class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="<?php echo home_url(); ?>">
          <img alt="Exploring Emerging Technologies for Lifelong Learning and Success Logo"
               height="55" src="/wp-content/uploads/brand.png">
          </a>
        </div>      <!-- End: navbar-header -->
	<div id="navbar" class="collapse navbar-collapse">
        <?php
          /* setting up the two nav menus (main and login)
           * here, 'menu' is the <ul> that holds the navbar elements
           * 'container' is the <div> that wraps the <ul>
           */
          wp_nav_menu(array(
            'menu' => 'main',
            'menu_class' => 'nav navbar-nav',
	    'container' => false,
            'depth' => 0,
            'fallback_cb' => 'wp_bootstrap_navwalker::fallback',
            'walker' => new wp_bootstrap_navwalker(),
          ));
	  wp_nav_menu(array(
	    'menu' => 'login',
	    'menu_class' => 'nav navbar-nav navbar-right',
	    'container' => false,
	    'depth' => 0,
            'fallback_cb' => 'wp_bootstrap_navwalker::fallback',
            'walker' => new wp_bootstrap_navwalker(),
	  ));
        ?>
	</div>
  </div>
  </nav>
