<?php
/*
Template Name: Search Page
*/
get_header();
?>

<?php
/*
 *   *** NOTE ABOUT SOME GLOBAL VARIABLES ***
 * MODULE_TAXONOMY is a constant defined in functions.php
 * its value is most certainly == "objectives"
 */

/* dropdown item contains the id, name, and parent name of a particular taxonomy 'term'
 * this could be an objective, keyword, subject, ... etc.
 */
final class DropdownItem {
	public $id;
	public $name;
	public $parent_name;
	function __construct($id_, $name_, $pname_) {
		$this->id = $id_;
		$this->name = $name_;
		$this->parent_name = $pname_;
	}
};
/*
 * The behavior of this page is different depending on whether a module is
 * selected or not (showing data for ALL modules).
 * ModuleHandlerBase is the common interface:
 *   get_all_posts - retrieve the universe of possible posts that can be shown
 *   objectives    - create list of 'DropdownItem' for the objectives dropdown
 *   render_module_hidden_inputs - create some hidden <input>s for Javascript,
 *                                 (module-id and module-key)
 *   show_default_posts - what to show on page load
 * and common data:
 *   module_list = list of the available modules (lifelong learning, creativity, ...)
 */
abstract class ModuleHandlerBase {
	private $module_list = [];
	function __construct($mods) {
		$this->module_list = $mods;
	}
	public function modules() {
		return $this->module_list;
	}
	abstract protected function get_all_posts();
	abstract protected function objectives();
	/* module-id and module-key <input> tags */
	abstract protected function render_module_hidden_inputs();
	abstract protected function show_default_posts($posts);
};
/*
 * SingleModuleHandler is used when a single module is selected.
 * we show only the objectives associated with that module.
 *   module_    - the module that was selected
 *   post_cache - list of all the available posts, used to select
 *                the available taxonomy terms in dropdown menus
 */
final class SingleModuleHandler extends ModuleHandlerBase {
	private $module_;
	private $post_cache;
	function __construct($mod, $mods) {
		parent::__construct($mods);
		$this->module_ = $mod;
		$this->cache_all_posts();
	}
	public function module() {
		return $this->module_;
	}
	/* get all posts which have objectives corresponding with this module.
	 * sorted by ratings, then by views
	 * these posts will be retrieved later; we store them in the object member $post_cache
	 */
	public function cache_all_posts() {
		/* get all posts which have objectives corresponding with this module */
		$children = get_term_children($this->module_->term_id, MODULE_TAXONOMY);
		$tax_query = array(
			array(
				'taxonomy' => MODULE_TAXONOMY,
				'terms' => array($this->module_->term_id),
				'operator' => 'IN',
				'include_children' => false,
			),
			array(
				'taxonomy' => MODULE_TAXONOMY,
				'terms' => $children,
				'operator' => 'IN',
				'include_children' => false,
			),
		);
		$args = array(
			'post_type' => 'resource',
			'tax_query' => $tax_query,
			'posts_per_page' => -1,
		);
		add_meta_query($args, 1, 1);  // sort by ratings, then views
		$wp_query = new WP_Query($args);
		if ($wp_query->have_posts()) {
			while ($wp_query->have_posts()) {
				$wp_query->the_post();
				$this->post_cache[] = $wp_query->post;
			}
			wp_reset_postdata();
		} else {
			$this->post_cache = null;
		}
	}
	public function get_all_posts() {
		return $this->post_cache;
	}
	/*
	 * Get the objectives that correspond with this module, create some DropdownItems
	 */
	public function objectives() {
		$objective_ids = get_term_children($this->module_->term_id, MODULE_TAXONOMY);
		if (is_wp_error($objective_ids) || empty($objective_ids)) {
			return null;
		}
		$objectives_ = [];
		foreach ($objective_ids as $oid) {
			$tmp = get_term_by('id', $oid, MODULE_TAXONOMY);
			$objectives_[] = new DropdownItem($tmp->term_id, $tmp->name,
						null);//exclude module to avoid redundancy
		}
		return $objectives_;
	}
	/* render the links to each module. they appear above search results and
	 * search parameters/dropdown menus
	 * make the currently selected module dark blue
	 * the links point to this page, but with ?module=XXX appended to the URL
	 */
	public function render_module_links($this_page_id) {
		$buttons = [];
		$link = get_page_link($this_page_id);
		foreach ($this->modules() as $m) {
			$active = (strcmp($m->slug, $this->module_->slug) == 0) ? " active" : '';
			$buttons[] = sprintf('<a class="btn btn-primary %s" href="%s?module=%s">%s</a>',
				$active, $link, $m->slug, $m->name);
		}
		return $buttons;
	}
	/* tell javascript the module-id and the taxonomy name (objectives) */
	public function render_module_hidden_inputs() {
		$fmt =<<<EOT
		<input id="module-id" type="hidden" value="%s">
		<input id="module-key" type="hidden" value="%s">
EOT;
		return sprintf($fmt, $this->module_->term_id, MODULE_TAXONOMY);
	}
	/* show all posts, sorted by rating */
	public function show_default_posts($posts) {
		foreach ($posts as $key => $p) {
			$id = $p->post_id;
			$posts[$key]->rating = get_post_meta($id, 'ratings_average');
		}
		usort($posts, function($a,$b) { return strcasecmp($a->post_title, $b->post_title); });
		// ONLY show the first three
		for ($i = 0; $i < 3; $i++) {
			echo searchpage_render_post($posts[$i]);
		}
	}
};
/* Handler when no module is selected */
final class NoModuleHandler extends ModuleHandlerBase {
	private $post_cache;
	function __construct($mods) {
		parent::__construct($mods);
		$this->cache_all_posts();
	}
	/* depending on query_var('random'); show random posts or everything */
	private function cache_all_posts() {
		$args = null;
		$random = get_query_var("random");
		if ($random && $random == "1") {// show random posts
			$args = array(
				'post_type' => 'resource',
				'orderby' => 'rand',
				'posts_per_page' => 3,
			);
		} else {        // show all post
			$args = array(
				'post_type' => 'resource',
				'posts_per_page' => -1,
			);
		}
		$wp_query = new WP_Query($args);
		if ($wp_query->have_posts()) {
			while ($wp_query->have_posts()) {
				$wp_query->the_post();
				$this->post_cache[] = $wp_query->post;
			}
			wp_reset_postdata();
		} else {
			$this->post_cache = null;
		}
	}
	public function get_all_posts() {
		return $this->post_cache;
	}
	/* retreive the objectives for ALL modules.
	 * when making the dropdown items, we set the visible name to include the module name
	 * so user is given some context of the objective */
	public function objectives() {
		$objectives_ = [];
		foreach ($this->modules() as $m) {
			$objective_ids = get_term_children($m->term_id, MODULE_TAXONOMY);
			if (is_wp_error($objective_ids) || empty($objective_ids)) {
				continue;
			}
			foreach ($objective_ids as $oid) {
				$tmp = get_term_by('id', $oid, MODULE_TAXONOMY);
				$objectives_[] = new DropdownItem($tmp->term_id, $tmp->name, $m->name);
			}
		}
		if (empty($objectives_)) {
			return null;
		}
		return $objectives_;
	}
	/* render the module links, no highlight. links point to current URL */
	public function render_module_links($this_page_id) {
		$buttons = [];
		$link = get_page_link($this_page_id);
		foreach ($this->modules() as $m) {
			$buttons[] = sprintf('<a class="btn btn-primary" href="%s?module=%s">%s</a>',
				$link, $m->slug, $m->name);
		}
		return $buttons;
	}
	public function render_module_hidden_inputs() {
		$fmt =<<<EOT
		<input id="module-id" type="hidden" value="0">
		<input id="module-key" type="hidden" value="%s">
EOT;
		return sprintf($fmt, MODULE_TAXONOMY);
	}
	/* tell user message about showing random items vs all items */
	public function make_default_post_toggle_link() {
		$html =<<<EOT
		Click here to switch between showing
		<a href="/search/?random=1" title="Show random items" id="random-items-link">Randomly Selected Tools/Resources</a>
		and
		<a href="/search/?random=0" title="Show all items" id="all-items-link">Showing All Items</a>
EOT;
		echo $html;
	}
	public function show_default_posts($posts) {
		foreach ($posts as $key => $p) {
			$id = $p->post_id;
			$posts[$key]->rating = get_post_meta($id, 'ratings_average');
		}
		usort($posts, function($a,$b) { return strcasecmp($a->post_title, $b->post_title); });
		foreach ($posts as $p) {
			echo searchpage_render_post($p);
		}
	}
};
/*
 * find dropdown item in a list of them, by ID
 * return: true if found, else false
 */
function find_dd_item($dd_item, $items) {
	foreach ($items as $i) {
		if ($dd_item->id == $i->id) {
			return true;
		}
	}
	return false;
}
/*
 * Renders the text that the user actually sees in the dropdown
 * put the module name in there if it is present (ie when there are multiple modules)
 */
function render_dropdown_item($item) {
	$fmt = '<option value="%d">%s</option>';
	$display = ($item->parent_name == null) ? $item->name : $item->parent_name.' - '.$item->name;
	return sprintf($fmt, $item->id, $display);
}
function dropdown_item_compare($a, $b) {      // for sorting
	$l = strtolower($a->name);
	$r = strtolower($b->name);
	return strcmp($l,$r);
}
// the ordering of $module_targets is important here
$modules = [];
$module_targets = array('lifelong-learning', 'communication-and-collaboration', 'creativity','critical-thinking');
foreach ($module_targets as $target) {
	$tmp = get_term_by('slug', $target, MODULE_TAXONOMY);
	if ($tmp !== false && $tmp->count > 0) { // only include modules for which there are posts to show
		$modules[] = $tmp;
	}
}
if (count($modules) == 0) {
	// something went wrong, maybe someone changed the name. Get the default modules
	//parent = 0 gets only the top-level terms (i.e. the 'modules')
	$modules = get_terms(array('taxonomy' => MODULE_TAXONOMY, 'hide_empty' => true, 'parent' => 0));
}
// check for the module in the URL, or default to no module
$query_var = get_query_var("module");
if ($query_var) {
        foreach ($modules as $m) {
                if (strcmp($m->slug, $query_var) == 0) {
			$module_handler = new SingleModuleHandler($m, $modules);
			break;
                }
        }
}
if (!isset($module_handler)) {
	$module_handler = new NoModuleHandler($modules);
}
/*
 * populate the drop-down menus with the appropriate fields
 * each taxonomy gets its own dropdown menu
 * dd_items is a map from taxonomy name -> list of terms (instances of the taxonomy)
 */
$posts = $module_handler->get_all_posts();
if ($posts == null) {
	echo render_error_loading_page();
	exit;
}
$tax_slugs = array('objectives', 'category', 'subjects', 'keywords');
$taxes = [];
$dd_items = [];
foreach ($tax_slugs as $t) {                            // inialize dd_items, taxes
	$tax_ = get_taxonomy($t);
	$taxes[] = $tax_;			        // array of taxonomy objects
	$dd_items[$tax_->name] = array();		// array of DropdownItems
}
$objectives = $module_handler->objectives();
if ($objectives == null) {
	echo render_error_loading_page();
	exit;
}
foreach ($objectives as $obj) {                        // append objectives to the dd_items
	$dd_items[MODULE_TAXONOMY][] = $obj;
}
/* for each of the other taxonomies, get the list of all possible terms, and fill
 * the dropdown menus with them
 */
foreach ($posts as $p) {
        $pid = $p->ID;
        foreach ($taxes as $t) {
                $tname = $t->name;
		if ($tname == MODULE_TAXONOMY) { // we already did this. Very important that we skip here.
			continue;
		}
		$tmp_terms = wp_get_post_terms($pid, $tname);
		foreach ($tmp_terms as $tmp) {
			$id_ = $tmp->term_id;
			$name_ = $tmp->name;
			$dd_item = new DropdownItem($id_, $name_, null); // null because we dont care about the parents for categories, keywords, etc.
			if (!find_dd_item($dd_item, $dd_items[$tname])) {
				$dd_items[$tname][] = $dd_item;
			}
		}
        }
}
/*
 * now sort each of the 'terms' alphabetically, case insensitive.
 * if the 'terms' are the objectives (tax_name == MODULE_TAXONOMY), sort by the module first, then by the objective names
 */
foreach (array_keys($dd_items) as $tax_name) {
	if ($tax_name == MODULE_TAXONOMY) {
		usort($dd_items[MODULE_TAXONOMY], function ($a,$b) {
			$parent_cmp = strcasecmp($a->parent_name, $b->parent_name);
			if ($parent_cmp === 0) {
				return strcasecmp($a->name, $b->name);
			}
			return $parent_cmp;
		});
	} else {
		usort($dd_items[$tax_name], 'dropdown_item_compare');
	}
}

/* DONE with initialization. */
?>
<div class="container">
  <div class="row">
    <div class="text-center">
      <h3 id="module-selection-message">Select a Module to Explore</h3>
      <div class="row">
        <div class="col-xs-12 col-sm-10 col-sm-offset-1 col-md-10 col-md-offset-1 col-lg-10 col-lg-offset-1">
          <?php
          /* Apply any post content that was specified by administrator */
          $this_page = get_page_by_title('Search');
          if (isset($this_page->post_content)) {
            echo apply_filters('the_content', $this_page->post_content);
          }
          ?>
        </div>
      </div><!-- row -->
      <div class="row">
        <div id="module-button-group" class="btn-group">
          <?php
            // create buttons to link to the 5 modules
            $buttons = $module_handler->render_module_links($this_page->ID);
            foreach ($buttons as $b) {
	            echo $b;
	    }
	  ?>
        </div><!--btn-group -->
      </div><!-- row -->
      <?php
	    /* link to toggle between random posts and show-all posts */
	    if ($module_handler instanceof NoModuleHandler) {
		    $module_handler->make_default_post_toggle_link();
	    }
      ?>
    </div><!-- module selection group -->
  </div> <!-- row -->
</div> <!-- container -->
<div class="full-height container"><!-- contains dropdown menus and the results pager -->
  <div class="row">
    <div class="col-md-3 parameters"> <!-- this is the left bar on the page, where users set query params -->
    <div class="container-fluid">
    <h4>Apply Filters</h4>
    <?php
    // DROP DOWN MENUS
        foreach ($taxes as $tax) {
                $tax_name = &$tax->name;
                $tax_display_name = &$tax->labels->name;
		// special case for 'Keywords'
		$tax_display_name = ($tax_display_name == "Keywords") ? "Keywords/Tags" : $tax_display_name;
		if (!empty($dd_items[$tax_name])) { // only display this dropdown menu if there are items to put in it
			?><div class="row"><?php
			printf('<select title="%s" id="%s" class="selectpicker" data-live-search="true" data-size="7" data-dropup-auto="false" multiple>',
			       $tax_display_name, $tax_name);
			foreach ($dd_items[$tax_name] as $dd_item) {
				echo render_dropdown_item($dd_item);
			}
			printf('</select>');
			?></div><?php
		}
        }
    ?>
    <div class="row"></div> <!-- for padding -->
    <!-- sorting options -->
    <div class="checkbox">
      <label><input id="sort_ratings" type="checkbox" value="0">Sort Highest Rated</label>
    </div>
    <div class="checkbox">
      <label><input id="sort_popular" type="checkbox" value="0">Sort by Popularity</label>
    </div>
    <!-- search box -->
    <div class="row">
    <h4>Search All Modules</h4>
    <div style="margin: 0 auto; width: 220px;">
      <div class="input-group">
        <input id="search_box" type="text" class="form-control" placeholder="Search...">
          <span class="input-group-btn">
            <button id="search_box_button" class="btn btn-primary" type="button">Go</button>
          </span>
      </div><!-- input-group -->
    </div>
    <!-- reset all current search parameters; reload the page -->
    <a id="reset" class="btn btn-primary" href="<?php echo get_page_link($this_page->ID); ?>"
    style="margin-top:1.5em">Reset Search Options</a>
    </div> <!-- row -->

    <!-- sneak in the module parameters (id and taxonomy name) here -->
    <?php
    echo $module_handler->render_module_hidden_inputs();
    ?>
    </div><!-- container-fluid -->
    </div><!-- col-md, parameters -->

    <div class="col-md-9">
      <div id="search-results">
	<?php
		$module_handler->show_default_posts($posts);
	?>
      </div><!-- search results -->
      <div class="text-center">
        <button class="btn btn-primary" id="load_more">Load More</button>
      </div>
    </div>
  </div><!-- row -->
</div><!-- /container -->

<?php
wp_reset_postdata();
?>
<?php get_footer(); ?>
