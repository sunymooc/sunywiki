<?php
get_header();
?>
<!-- index.php is shown when there is no other appropriate page template
     it is shown when the page does not exist. -->
<div class="full-height container">
<div class="row">
  <div class="col-md-8 col-md-offset-2" style="margin-top: 40px">
    <h2 class="text-center">This page does not exist! Please visit our <a href="/index.php">homepage</a>. Or use the navbar above to navigate our site.</h2>
  </div>
</div>
</div>
<?php get_footer(); ?>
