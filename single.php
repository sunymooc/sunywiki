<?php get_header(); ?>

<!-- single.php specifies what will display when a user
 visits the page for a particular tool -->

<div class="full-height container">
  <div class="row">
    <!-- post thumbnail and title -->
    <div class="col-md-12">
      <?php
        $post_thumbnail_markup = default_thumbnail(array(80,80), array("style" => "float:left"));
        if (has_post_thumbnail($post)) {
            $post_thumbnail_markup = get_the_post_thumbnail($post, array(80,80), array("style" => "float:left"));
        }
        echo $post_thumbnail_markup;
      ?>
      <h1 class="post-title"><?php the_title(); ?></h1>
    </div> <!-- col-md -->
    <!-- ratings, views, tags, etc. -->
    <?php
      $this_page = get_queried_object();
      $pid = $this_page->ID;
      if (function_exists('the_ratings')) {
        echo '<div class=\"col-md-12\">';
        the_ratings();
        echo '</div>';
      }
      if (function_exists('mviews_get_post_views')) {
        $c = (int) mviews_get_post_views($pid);
        echo '<div class=\"col-md-12\">';
        printf("%d views<br/>", $c);
        echo '</div>';
        mviews_inc_post_views($pid);
      }
      echo '<div class=\"col-md-12\">';
      edit_post_link("Edit this page", null, null, $pid);
      echo '</div>';
      /* display taxonomy terms for this post */
      function make_tag_item($name) {
        return "<div class=\"post-tag\">$name</div>";
      }
      $tax_names = array('objectives', 'category', 'subjects', 'keywords');
      foreach ($tax_names as $tax) {
        $terms = wp_get_post_terms($pid, $tax);
        if (count($terms)) {
          echo '<div class="col-sm-6 col-md-12">';// the full width column to display this taxonomy in
          $tax = (strcmp($tax, "category") == 0) ? "categories" : $tax;
          $tax = ucfirst($tax);
          // put the taxnomy name in a subcolumn of width 1. the next 11 columns go to the terms
          // this ensures that they are aligned nicely and when terms wrap around they do not
          // end up directly under the taxonomy name
          echo "<div class=\"col-md-1\"><strong>$tax: </strong></div>";
          echo "<div class=\"col-md-11\">";
          foreach ($terms as $term) {
            echo make_tag_item($term->name) . ' ';
          }
          echo '</div></div>';
        }
      }
    ?>
    <div class="col-md-12">
      <hr>
    </div>
  </div><!-- /row (just below full-height container) -->
  <!-- this ends the title section, now just show the_content -->
  <div class="row">
    <div class="post-content-wrapper">
    <div class="col-xs-12 col-lg-10 col-lg-offset-1">
      <?php
        while (have_posts()) {
          the_post();
          the_content();
        }
        wp_reset_postdata();
      ?>
    </div>
    </div>
  </div> <!-- row -->
</div> <!-- container -->
<?php get_footer(); ?>
