<footer id="footer" class="footer">
  <div id="footer-nav" class="navbar">
    <div class="container-fluid">
      <div class="row vert-align">
        <div class="col-sm-10 col-sm-offset-1 col-md-12 col-lg-10 col-lg-offset-1">
          <div class="row vert-align">
            <div class="col-xs-6 col-sm-4 col-sm-offset-3 col-md-2 col-md-offset-1 col-lg-2 col-lg-offset-2">
              <ul>
                <li><a href="/" class="footer-link-dark">Home</a></li>
                <li><a href="/about" class="footer-link-light">About</a></li>
                <li><a href="/certificate-and-badges" class="footer-link-light">Certificate & Badges</a></li>
                <li><a href="/faq" class="footer-link-light">FAQ</a></li>
                <li><a href="/acknowledgements" class="footer-link-light">Acknowledgements</a></li>
                <?php
		/* Only show 'edit-this-page' if the user is an administrator */
                if (current_user_can("administrator")) {
	                $this_page = get_queried_object();
	                $pid = $this_page->ID;
	                edit_post_link("Edit this page", null, null, $pid);
	        }
                ?>
                <li><a href="/search" class="footer-link-dark">Discover</a></li>
                <li><a href="/exercises" class="footer-link-light">Discovery Exercises</a></li>
              </ul>
            </div>
            <div class="col-xs-6 col-sm-4 col-md-3 col-lg-2">
              <ul>
                <li><a href="/emtechwiki-contributor-instructions" class="footer-link-dark">Contributing</a></li>
                <li><a href="/guidelines">Guidelines</a></li>
                <li><a href="/emtechwiki-contributor-instructions" class="footer-link-light">Instructions</a></li>
                <li><a href="/emtechwiki-terms-of-service">Terms of Service</a></li>
              </ul>
              <ul>
                <li><a href="https://www.coursera.org/learn/emerging-technologies-lifelong-learning" class="footer-link-dark">#EmTechMOOC</a></li>
                <li><a href="/contact-us" class="footer-link-light">Contact</a></li>
              </ul>
            </div>
            <div class="col-xs-6 col-sm-4 col-sm-offset-3 col-md-2 col-md-offset-0 col-lg-2">
              <a href="https://credly.com/u/sunycpd"><img alt="Credly - Credit Issuer" height="36" width="100" src="/wp-content/uploads/credly-credit-issuer.png"></a>
              <br />
              <a href="https://creativecommons.org/licenses/by-nc/4.0/"><img alt="Creative Commons License - (CC BY-NC 4.0)" height="32" width="100" src="/wp-content/uploads/copyright.png"></a>
            </div>
            <div class="col-xs-5 col-sm-3 col-md-2">
              <a href="https://suny.edu"><img alt="State University of New York (SUNY)" class="img-reponsive" style="width:100%;" src="/wp-content/uploads/logo-suny_footer.png"></a>
            </div>
          </div><!-- row -->
        </div><!-- col* -->
      </div><!-- row -->
    </div><!-- container fluid -->
  </div><!-- navbar -->
</footer>
<?php wp_footer(); ?>
</body> <!-- close off tags left open by other templates -->
</html>
