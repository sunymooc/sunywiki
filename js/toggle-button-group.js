var wide_screen = 640;
function toggle_button_group() {
	var width = $(window).width();
	if (width < wide_screen) {
		$("#module-button-group").addClass("btn-group-vertical").removeClass("btn-group");
	} else {
		$("#module-button-group").addClass("btn-group").removeClass("btn-group-vertical");
	}
}
$(document).ready(function() {
	toggle_button_group();
	window.addEventListener('resize', toggle_button_group);
});
