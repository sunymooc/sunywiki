function list_mobile()
{
	var w = $(window).width();
	if (w < 572) {
		$(".post-content-wrapper ul, .post-content-wrapper ol").addClass("list-mobile");
	} else {
		$(".post-content-wrapper ul, .post-content-wrapper ol").removeClass("list-mobile");
	}
}
$(document).ready(function() {
	list_mobile();
	window.addEventListener('resize', list_mobile);
});