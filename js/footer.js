//var footer_wide_screen = 640;
var footer_wide_screen = 1000;
function toggle_footer_vert_align() {
	var width = $(window).width();
	if (width < footer_wide_screen) {
		$('#footer .row').removeClass('vert-align');
	} else {
		$('#footer .row').addClass('vert-align');
	}
}

$(document).ready(function() {
	toggle_footer_vert_align();
	window.addEventListener('resize', toggle_footer_vert_align);
});
