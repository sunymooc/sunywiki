/*
 * wp-user-manager does not allow us to set custom CSS classes
 * for the <input>'s, so we do it manually.
 * also, we make the submit button look better
 */
$(document).ready(function() {
	$("form").find("input[type='text'],input[type='password'],input[type='email']").each(function () {
		$(this).removeClass(); //this removes ALL the classes
		$(this).addClass("form-control");
	});
	$("form").find("input[type='submit']").each(function () {
		$(this).removeClass();
		$(this).addClass("btn btn-primary");
	});
});
